#!/bin/bash
set -e


[[ -f /var/www/html/index.html ]] && rm -r /var/www/html/*


# Modificar los valores del fichero default.conf 'ServerAlias', 'ServerName' y Serveradmin
if [ -f /etc/apache2/sites-available/000-default.conf ]; then
	rm /etc/apache2/sites-available/000-default.conf
	cp /app/default.conf /etc/apache2/sites-available/default.conf
	a2ensite default.conf
	echo 'ServerName emulatrix.com' >> /etc/apache2/apache2.conf
	cp /app/emulatrix.tgz /var/www/html/
	cd /var/www/html && tar -zxvf emulatrix.tgz
fi

#inicia apache
#/etc/init.d/apache2 start
apachectl -D FOREGROUND
/etc/init.d/apache2 reload

exec "$@"
