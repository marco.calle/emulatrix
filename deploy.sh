#!/bin/bash

##Creación de volumen persistente
microk8s.kubectl apply -f emulatrix-pv.yaml

##Creación de reclamo de espacio
microk8s.kubectl apply -f emulatrix-pvc.yaml
##Implementar deploy de Emulatrix
microk8s.kubectl apply -f emulatrix-deployment.yaml
##Exponer IP del cluster por puerto aleatorio
microk8s.kubectl expose deployment emulatrix-deployment --type=NodePort

echo 'Acceder por puerto: '$(microk8s.kubectl get service | grep emulatrix | cut -d ':' -f 2 | cut -d '/' -f 1)
