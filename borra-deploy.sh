#!/bin/bash
microk8s.kubectl delete service emulatrix-deployment && \
microk8s.kubectl delete deployment emulatrix-deployment && \
microk8s.kubectl delete pvc emulatrix-pv-claim && \
microk8s.kubectl delete pv emulatrix-pv && \
microk8s.kubectl delete pod --all 

rm -r /mnt/emulatrix-pv && echo 'Directorio mnt/emulatrix-pv borrado'
