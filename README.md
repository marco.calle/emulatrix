# Emulatrix - Multi Emulador
## Actualización tecnológica - TP - Kubernetes

El proyecto esta basado en el multi emulador Emulatrix https://github.com/lrusso/Emulatrix.git

Se implementa un cluster de 3 replicas con usando deployment.
Se generó la imagen mcalle86/emulatrix:1.1 que tiene webserver apache y emulatrix (carpeta emu contiene Dockerfile, entrypoint.sh, default.conf y emulatrix.tgz)

YAML del proyecto e implementación

### Volumen persistente
> emulatrix-pv.yaml
Se montará volumen en /mnt/emulatrix-pv
#### Comando para implementar
> microk8s.kubectl apply -f emulatrix-pv.yaml

### Reclamo de espacio
> emulatrix-pvc.yaml
#### Comando para implementar
> microk8s.kubectl apply -f emulatrix-pvc.yaml

### Deploy de la aplicación
> emulatrix-deployment.yaml
#### Comando para implementar
> microk8s.kubectl apply -f emulatrix-deployment.yaml

### Exponer IP del cluster por puerto aleatorio, 
> microk8s.kubectl expose deployment emulatrix-deployment --type=NodePort

`Para acceder a la app se debe utilizar http://ip_vm:puertoNodeport`

### Ver el puerto asignado para acceder
> microk8s.kubectl get service | grep emulatrix | cut -d ':' -f 2 | cut -d '/' -f 1

`Acceder a la aplicacion mediante http://ip_host:puerto`

### Script deployment modo desatendido, al ejecutar el script se realizará toda la implementación
> deploy.sh

`Al finalizar la implementacion informará el puerto de conexión`

### Ver todos los objetos creados
> microk8s.kubectl get service && microk8s.kubectl get deployment && microk8s.kubectl get pv && microk8s.kubectl get pvc && microk8s.kubectl get pods

### Escalar replicas 
> kubectl scale deployment emulatrix-deployment --replicas 5

### Borrar todos los objetos (volumen persistente, reclamo, deployment, service y pods)
> borra-deploy.sh

### Descargar la rom de ejemplo en el equipos HOST para probar el emulador
> Ultimate_Mortal_Kombat_3.md
